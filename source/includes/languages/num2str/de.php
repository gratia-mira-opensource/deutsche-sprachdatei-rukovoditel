<?php
$data = array(
//zero value
		'nul' => 'null',
		//form 1-9
		'ten' =>
		array(
				array('','eins', 'zwei', 'drei', 'vier', 'fünf', 'sechs', 'sieben', 'acht', 'neun'),
				array('','eins', 'zwei', 'drei', 'vier', 'fünf', 'sechs', 'sieben', 'acht', 'neun'),
		),
		//from 10-19
		'a20' =>
		array('zehn', 'elf', 'zwölf', 'dreizehn', 'vierzehn', 'fünfzehn', 'sechzehn', 'siebzehn', 'achtzehn', 'neunzehn'),
		//from 20-90
		'tens' =>
		array(2=>'zwanzig', 'dreißig', 'vierzig', 'fünfzig', 'sechzig', 'siebzig', 'achtzig', 'neunzig'),
		//from 100-900
		'hundred' =>
		array('','einhundert', 'zweihundert', 'dreihundert', 'vierhundert', 'fünfhundert', 'sechshundert', 'siebenhundert', 'achthundert', 'neunhundert'),
		//units
		'unit' =>
		array( // Units
				array('cent', 'cent', 'cents',	 1),
				array('euro'   ,'euros'   ,'euros'    ,0),
				array('tausend', 'Tausende', 'Tausende'     ,1),
				array('Million' ,'Million','Millionen' ,0),
				array('Milliarde', 'Milliarde', 'Milliarden',0),
		)
);