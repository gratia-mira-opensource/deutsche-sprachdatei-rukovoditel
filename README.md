Die offizelle deutsche Sprachdatei von [Rukovoditel](https://www.rukovoditel.net/download.php), dem einfachen Project Management App Designer.

Übersetzt von brave.

[Über Rukovoditel](https://gdlschmiede.de/kb/rukovoditel-allgemein/)   
[Übersetzungen & Installation](https://www.rukovoditel.net/translations.php)   
[Forum](https://forum.rukovoditel.net/)   
[Dokumentation](https://docs.rukovoditel.net/)   
